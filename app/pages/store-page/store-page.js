import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import { FormBuilder, ControlGroup } from '@angular/common';
/*
  Generated class for the StorePagePage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'build/pages/store-page/store-page.html',
})
export class StorePagePage {
  static get parameters() {
    return [[NavController] , [FormBuilder] ];
  }

  constructor(nav ,formBuilder) {
    this.nav = nav;
    this.myData_array = null;
    this.myForm = formBuilder.group({
      'storeOwner': '',
      'logoUrl': '',
      'phone': '',
      'address.city': '',
      'address.state': '',
      'address.zipCode': '',
      'address.lat': '',
      'address.long': ''
    });
    this.renderLocalStorage();
    this.map = null;

  }



  ionViewLoaded() {
    var mapEle = document.getElementById('map');
    var map = new google.maps.Map(mapEle, {
      center: {lat: -34.397, lng: 150.644},
      zoom: 8
     });

    google.maps.event.addListenerOnce(map, 'idle', () => {
      mapEle.classList.add('show-map');
      console.log("almgwary ..  ",map);
    });

  };


  onSubmit(formData) {
    console.log('Form submitted is ', formData);
    this.myData = formData;
    this.saveToLocalStorage(formData);

  }

  // push json item to local storage
  saveToLocalStorage(jsonItem){

    // Retrieve the object from storage
    var retrievedObject = localStorage.getItem('my_store_data_array');
    var array = JSON.parse(retrievedObject);
    if(array === null) array = [] ;
    array.push(jsonItem) ;

    // Put the object into storage

    localStorage.setItem('my_store_data_array', JSON.stringify(array));

  }


  renderLocalStorage(){
    var retrievedObject = localStorage.getItem('my_store_data_array');
    this.myData_array = JSON.parse(retrievedObject);

  }




}
