import {Component} from '@angular/core';
import {NavController,NavParams} from 'ionic-angular';
import {CategoryItem} from "../../providers/category-item/category-item"
import {ItemDetailPage} from "../../pages/item-detail/item-detail"
/*
  Generated class for the CategoryPagePage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  templateUrl: 'build/pages/category-page/category-page.html',
  providers: [CategoryItem]
})
export class CategoryPagePage {
  static get parameters() {
    return [[NavController],[NavParams],[CategoryItem]];
  }

  constructor(nav,navParams, _categoryItem) {
    this.nav = nav;
    this.category =navParams.get("category") ;
    this.categoryItems = []  ;
    _categoryItem.loadItems(this.category).subscribe(data => {

      this.categoryItems = data;
      //console.log("Category:"+this.category + "   Data = [" + this.categoryItems + " ]");

    });
  }


  displayItemPage(item){
    console.log("gotoItem  = [" +item + " ]");
    this.nav.push(ItemDetailPage , {
      item: item
    });
  }
}
