import {Component,Injectable, bind} from '@angular/core';
import {NavController} from 'ionic-angular';
import {ScientificFactsPage} from '../scientific-facts-page/scientific-facts-page';

import {CategoryPagePage} from '../category-page/category-page';

import {CategoriesList} from '../../providers/categories-list/categories-list' ;
import{StorePagePage} from '../store-page/store-page' ;

@Component({
  templateUrl: 'build/pages/home-page/home-page.html',
  providers:[CategoriesList]
})
export class HomePage {
  static get parameters() {
    return [[NavController], [CategoriesList]];
  }


  constructor(_navController , _categoriesList ) {
    this._navController = _navController;
    this.categories    =  ['Empty',] ;

     _categoriesList.load().subscribe(data => {

          this.categories = data.data;
          console.log("Model Data = [" + this.categories + " ]");

     });

  }

    goToCategoyPage(category){
        //console.log("goto  = [" +category + " ]");
        this._navController.push(CategoryPagePage , {
            category: category
        });
    }



    goToFactsPage(){
        this._navController.push(ScientificFactsPage);
    }

    goToStorePage(){
        this._navController.push(StorePagePage);
    }


}
