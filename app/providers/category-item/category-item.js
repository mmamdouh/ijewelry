import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';
/*
  Generated class for the CategoryItem provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class CategoryItem {
  static get parameters(){
    return [[Http]]
  }

  constructor(http) {
    this.http = http;
  }

  loadItems(category) {
    //console.log("Loading items  of category: " + category) ;
    return this.http.get('build/mock/'+ category+'_items.json')
        .map(res => res.json())  ;
  }
}

